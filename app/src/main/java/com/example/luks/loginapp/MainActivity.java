package com.example.luks.loginapp;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.luks.loginapp.hilos.HiloLoginWS;
import com.example.luks.loginapp.hilos.HiloRegistroWS;

public class MainActivity extends AppCompatActivity {

    private EditText txtEmail, txtPassword;
    private Button btnLogin, btnRegistrar;
    private LinearLayout panel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        panel = findViewById(R.id.panel);
    }

    public void ejecutaLogin(View v) {
        if (!txtPassword.getText().toString().isEmpty() && !txtEmail.getText().toString().isEmpty()) {
            HiloLoginWS hl = new HiloLoginWS(this);
            hl.execute(txtEmail.getText().toString(), txtPassword.getText().toString());
        }else{
            Toast.makeText(this, "Hay que rellenar tanto el usuario como la contraseña", Toast.LENGTH_SHORT).show();
        }
    }

    public void ejecutaRegistro(View v) {
        if (txtPassword.getText().toString().length() > 4) {
            if (!txtEmail.getText().toString().isEmpty()) {
                HiloRegistroWS hl = new HiloRegistroWS(this);
                hl.execute(txtEmail.getText().toString(), txtPassword.getText().toString());
            } else {
                Toast.makeText(this, "Ingrese un nombre de usuario!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "La contraseña tiene que tener al menos 4 dígitos", Toast.LENGTH_SHORT).show();
        }

    }

    public LinearLayout getPanel() {
        return panel;
    }

    public EditText getTxtEmail() {
        return txtEmail;
    }

    public EditText getTxtPassword() {
        return txtPassword;
    }
}
