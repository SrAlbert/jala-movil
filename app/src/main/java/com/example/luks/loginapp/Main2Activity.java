package com.example.luks.loginapp;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    private TextView token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        token = findViewById(R.id.token);
        SharedPreferences sp = getSharedPreferences("datos", MODE_PRIVATE);
        token.setText(sp.getString("token", "No hay token!"));
    }
}
