package com.example.luks.loginapp.hilos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.luks.loginapp.Main2Activity;
import com.example.luks.loginapp.MainActivity;
import com.example.luks.loginapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class HiloRegistroWS extends AsyncTask<String, Void, String> {

    private MainActivity activity;
    private ProgressDialog dialog;

    public HiloRegistroWS(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.getPanel().removeAllViews();
        dialog = new ProgressDialog(activity);
        dialog.setTitle(R.string.cargando);
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        dialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            //String url = "http://192.168.43.83:8080/jala/rest/oauth/register?username=" + strings[0]
            String url = "http://192.168.13.28:8080/jala/rest/oauth/register?username=" + strings[0]
                    + "&password=" + strings[1] + "&level=1";
            return callRestWSByPut(url);
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        dialog.dismiss();
        try {
            JSONObject jo = new JSONObject(s);
            int errorCode = (int) jo.get("errorCode");
            if (errorCode == 0) {
                Toast.makeText(activity, "Usuario creado correctamente" , Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public String callRestWSByPost(String url) throws IOException {
        URL parURL = new URL(url);

        HttpURLConnection urlConnection = (HttpURLConnection) parURL.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        urlConnection.setRequestProperty("Content-type", "application/x-www-form-urlenCcoded");
        urlConnection.setAllowUserInteraction(true);
        urlConnection.connect();

        StringBuilder sb = new StringBuilder();
        InputStream in = ((HttpURLConnection) urlConnection).getInputStream();
        int length = urlConnection.getContentLength();
        for (int n = 0; n < length; n++) {
            sb.append((char) in.read());
        }
        return sb.toString();
    }

    public String callRestWSByPut(String url) throws IOException {
        URL parURL = new URL(url);
        HttpURLConnection urlConnection = (HttpURLConnection) parURL.openConnection();
        urlConnection.setRequestMethod("PUT");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        urlConnection.setRequestProperty("Content-type", "application/x-www-form-urlenCcoded");
        urlConnection.setAllowUserInteraction(true);
        urlConnection.connect();

        StringBuilder sb = new StringBuilder();
        InputStream in = ((HttpURLConnection) urlConnection).getInputStream();
        int length = urlConnection.getContentLength();
        for (int n = 0; n < length; n++) {
            sb.append((char) in.read());
        }
        return sb.toString();
    }

    public String callRestWSByGet(String url) throws IOException {
        URL urlws = new URL(url);
        URLConnection uc = urlws.openConnection();
        uc.connect();
        //Creamos el objeto con el que vamos a leer
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        String inputLine;
        String contenido = "";
        while ((inputLine = in.readLine()) != null) {
            contenido += inputLine + "\n";
        }
        in.close();
        return contenido;
    }
}
